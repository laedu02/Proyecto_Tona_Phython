import requests
import json
import matplotlib.pyplot as plt
import numpy as np
tona = requests.get("https://api.bitbucket.org/2.0/repositories/fabtl10/Elevate/commits")


data = json.loads(tona.content)

i = 0
F = 0
E = 0
for value in data['values']:
    autor=value["author"]
    raw=autor["raw"]
    if(value['type'] == "commit"):
        i = i + 1
    if(autor['raw'] == "Fabian <fgandarilla@uabc.edu.mx>"):
        F = F + 1 
    if(autor['raw'] == "Fabian Gandarilla Lopez <fgandarilla@uabc.edu.mx>"):
        F = F + 1 
    if(autor['raw'] == "Eduardo Prieto Campos <al351490@loginfc.lan>"):
        E = E + 1 
    if(autor['raw'] == "eduardo <eduardo.prieto.campos@uabc.edu.mx>"):
        E = E + 1 
Nombres=("Fabian","Eduardo")
commits=(F,E)
posiciones = np.arange(len(Nombres))
plt.yticks(posiciones, Nombres)
plt.barh(posiciones,commits)
plt.xlabel('Numero de commits')
plt.show()

print("Numero de commits: " + str(i))
print("Numero de commits Fabi: " + str(F))
print("Numero de commits Yo mero: " + str(E))

